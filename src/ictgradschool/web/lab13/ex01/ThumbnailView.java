package ictgradschool.web.lab13.ex01;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * Created by mmiz318 on 10/01/2018.
 */
public class ThumbnailView extends HttpServlet {
    private boolean isMultipart;
    private String filePath;
    private int maxFileSize = 50 * 1024 * 1024;
    private int maxMemSize = 4 * 1024 * 1024;
    private File file ;

//    public void init( ){
//        // Get the file location where it would be stored.
//        filePath = getServletContext().getInitParameter("file-upload");
//
//    }

    public void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, java.io.IOException {
        ServletContext servletContext = getServletContext();
        filePath = servletContext.getRealPath("/Uploaded-Photos");
        System.out.println("initial filePath " + filePath + "\\");

        Map<String, String[]> picName = request.getParameterMap();

        for(String key : picName.keySet()){
            System.out.println("in for loop");
            String[] value = picName.get(key);
            System.out.println("<p>" + key + "</p><p>" + Arrays.toString(value) + "</p>");
        }

        // Check that we have a file upload request
        isMultipart = ServletFileUpload.isMultipartContent(request);
        response.setContentType("text/html");
        java.io.PrintWriter out = response.getWriter( );

        if( !isMultipart ) {
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet upload</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<p>No file uploaded</p>");
            out.println("</body>");
            out.println("</html>");
            return;
        }

        DiskFileItemFactory factory = new DiskFileItemFactory();

        // maximum size that will be stored in memory
        factory.setSizeThreshold(maxMemSize);

        // Location to save data that is larger than maxMemSize.
        factory.setRepository(new File("c:\\temp"));

        // Create a new file upload handler
        ServletFileUpload upload = new ServletFileUpload(factory);

        // maximum file size to be uploaded.
        upload.setSizeMax( maxFileSize );

        try {
            // Parse the request to get file items.
            List fileItems = upload.parseRequest(request);

            // Process the uploaded file items
            Iterator i = fileItems.iterator();

            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet upload</title>");
            out.println("</head>");
            out.println("<body>");

            while ( i.hasNext () ) {
                FileItem fi = (FileItem)i.next();
                //System.out.println("Fileitem" + fi);
                if ( !fi.isFormField () ) {
                    // Get the uploaded file parameters
                    String fieldName = fi.getFieldName();
                    String fileName = fi.getName();
                    System.out.println("fileName " + fileName);
                    String contentType = fi.getContentType();
                    boolean isInMemory = fi.isInMemory();
                    long sizeInBytes = fi.getSize();

                    // Write the file
                    if( fileName.lastIndexOf("\\") >= 0 ) {
                        file = new File( filePath + fileName.substring( fileName.lastIndexOf("\\"))) ;
                    } else {
                        file = new File( filePath + fileName.substring(fileName.lastIndexOf("\\")+1)) ;
                        System.out.println("filepath in else block: " + filePath + fileName.substring(fileName.lastIndexOf("\\")+1));
                    }
                    fi.write( file ) ;


                    //String[] pic = picName.get(1);

                    //out.println("Uploaded Filename: " + pic[0] + "<br>");
                    System.out.println("HTML tag: <img src='" + filePath + fileName + "'>");
                    out.println("<img src='Uploaded-Photos" + fileName + "'>");
                }
                else {
                    //System.out.println(fi);
                    out.println("<p>File name: " + fi.getString() + "</p>");
                }
            }
            out.println("</body>");
            out.println("</html>");
        } catch(Exception ex) {
            System.out.println(ex);
        }
    }

    public void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, java.io.IOException {

        throw new ServletException("GET method used with " +
                getClass( ).getName( )+": POST method required.");
    }









//    @Override
//    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
//        resp.setContentType("text/html");
//
//        PrintWriter out = resp.getWriter();
//        out.println("<!DOCTYPE html><head><title>Title</title></head><body>");
//
//
//
//        DiskFileItemFactory factory = new DiskFileItemFactory();
//
//
//
//        factory.setSizeThreshold(5000000);
//
//        File repository = (File) servletContext.getAttribute("javax.servlet.context.tempdir");
//        factory.setRepository(repository);
//
//        ServletFileUpload upload = new ServletFileUpload(factory);
//
//        try {
//            List<FileItem> items = upload.parseRequest(req);
//            out.println("<p>Hello</p>");
//        } catch (FileUploadException e) {
//            e.printStackTrace();
//        }
//
//
//        out.println("</body></html>");
//    }
}
