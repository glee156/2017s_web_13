package ictgradschool.web.lab13.ex01;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

/**
 * Created by glee156 on 10/01/2018.
 */
public class ImageGalleryViewer extends HttpServlet{

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        ServletContext servletContext = getServletContext();
        String fullPhotoPath = servletContext.getRealPath("/Photos");

        File file = new File(fullPhotoPath);
        String[] arrayOfFile = file.list();

        resp.setContentType("text/html");

        PrintWriter out = resp.getWriter();
        out.println("<!DOCTYPE html><head><title>Title</title></head><body>");

        String href = null;
        long size = 0;

        for(String s: arrayOfFile){
            //int start = s.indexOf("2017s_web_lab_13");
            System.out.println(s);
            String name = null;


            //s = s.substring(start);
            if(!s.contains("thumbnail")){
               href = "/Photos/" + s;
               File img = new File(fullPhotoPath + "/" + s);
               size = img.length();
            }

            else{
                name = s.replace("_thumbnail.png", "");
                name = name.replaceAll("_", " ");
                out.println("<h1>" + name + " " + size + " kilobytes</h1>");
                out.println("<a href='" + href + "'><img src='/Photos/" + s + "'></a>");
            }
        }

        out.println("</body></html>");

    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doPost(req, resp);
    }
}
